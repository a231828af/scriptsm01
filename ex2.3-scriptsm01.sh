#! /bin/bash
#@edt ASIX-M01 Curs 2023-2024
#Febrer 2024
#Exercici 2.3 d'scripts bàsics
#Descripció:Processar arguments que són matricules:
#a) Llistar les vàlides, del tipus: 9999-AAA.
#b) stdout les que són vàlides, per stderr les no vàlides. Retorna de status el
#número d’errors (de no vàlides). 
#-----------------------------------------------------------------------------
#1)Validar arguments
ERR_ARGS=1
if [ $# -eq 0 ]
then
 echo "Error: Num args incorrecte"
 echo "Usage: $0 arg"
 exit $ERR_ARGS
fi

#2)xixa 
num=0
for matricula in $*
do
	echo $matricula | grep -E "^[0-9]{4}-[A-Z]{3}$"
		if [ $? -ne 0 ];then
			echo "Error $matricula no valid"&> /dev/stderr
			((num++))
					
		fi
done
exit $num


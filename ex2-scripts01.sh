#! /bin/bash
#@edt ASIX-M01 Curs 2023-2024
#Febrer 2024
#Exercici 2 d'scripts bàsics
#Descripció:Mostar els arguments rebuts línia a línia, tot numerànt-los.
#---------------------------------------------------------
ERR_NARGS=1
#1) Valida que hi ha 1 o més arguments
if [ $# -eq 0 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args"
  exit $ERR_NARGS
fi
#2) xixa
num=1
for arg in $*
do
  echo "$num: $arg"
  num=$((num+1))
done
exit 0

#! /bin/bash
#@edt ASIX-M01 Curs 2023-2024
#Febrer 2024
#Descripció: dir els dies que té un mes
#Synopsis: prog mes
#	a) validar rep un arg
#	b)validar mes  [1-12]
#	c)xixa
#---------------------------------------------------------
ERR_NARGS=1
ERR_MES=2
# 1) Valida que hi ha un argument
if [ $# -eq 0 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 mes"
  exit $ERR_NARGS
fi

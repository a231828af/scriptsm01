#! /bin/bash
#@edt ASIX-M01 Curs 2023-2024
#Febrer 2024
#Exercici 2.5 d'scripts bàsics
#Descripció:Processar stdin mostrant per stdout les línies de menys de 50 caràcters.
#----------------------------------------------------------------------------
#1) XIXA
while read -r line
do
	echo $line | grep -E "^.{,50}$"
done


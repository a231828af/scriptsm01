#! /bin/bash
#@edt ASIX M01-ISO
#prog dir...
#a) rep un arg i és un directori i es llista
#b) llistar numerant els elements del dir
#c) per cada element dir si és dir, regular o altra cosa
#d) es reben n directoris
#------------------------------------------------------------
ERR_NARGS=1
ERR_DIR=2
#1) validar que hi ha un arg
if [ $# -eq 0 ]; then
	echo "Error: num args incorrecte"
	echo "Usage: $0 dir..."
	exit $ERR_NARGS
fi
dir=$1
#2) Iterar per cada argument
for dir in $*
do
	if [ ! -d $dir ]; then
	  echo "ERROR: $dir no és un directori" >> /dev/stderr
	else
	  llista=$(ls $dir)
	  echo "Llista: $dir-----"
	  for elem in $llista
	  do
  	  if [ -f $dir/$elem ]; then
	    echo "$elem és un regular file"
  	  elif [ -d $dir/$elem ]; then
	    echo "$elem és un directori"
  	  else
	    echo "$elem és una altra cosa"
	  fi
 done	  
fi
done
exit 0

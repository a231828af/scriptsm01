#! /bin/bash
#@edt ASIX-M01 Curs 2023-2024
#Febrer 2024
#Descripció: exemples bucle for
#---------------------------------------------------------
#8) llistar tots els logins ordenats i numerats
logins=$(cut -d: -f3 /etc/passwd | sort)
num=1
for login in $logins
do
	echo"$num: $logins"
	num=$((num+1))
done
exit 0

#7) llistar numerant les linies
llistat=$(ls)
num=1
for elem in $llistat
do
	echo "$num: $elem"
         num=$((num+1))
done
exit 0


#6) itera echo "$num: $arg"
llistat=$(ls)
for elem in $llistat
do
	echo "$elem"
done
exit 0

#5) numerar arguments
num=1
for arg in $*
do
  echo "$num: $arg"
  num=$((num+1))
exit 0

#5) numerar arguments
num=1
for arg in $*
do
  echo "$num: $arg"
  num=$((num+1))
 per cada un dels valors que generi l'ordre ls
llistat=$(ls)
for elem in $llistat
do
	echo "$elem"
done
exit 0

#5) numerar arguments
num=1
for arg in $*
do
  echo "$num: $arg"
  num=$((num+1))
done
exit 0

#4) $@ expadeix $* no
for arg "$@"
do
  echo "$arg"
done
exit 0


#3) Iterar per la llista d'arguments
for arg in $*
do
  echo "$arg"
done
exit 0
#1) iterar noms
for nom in "pere" "marta" "anna" "pau"
do
	echo "$nom"
done
exit 0
#2) iterar noms
for nom in "pere" "marta" "anna" "pau"
do
	echo "$nom"
done
exit 0

#! /bin/bash
#@edt ASIX-M01 Curs 2023-2024
#Febrer 2024
#Descripció: dir els dies que té un mes
#Synopsis: prog mes
#	a) validar rep un arg
#	b)validar mes  [1-12]
#	c)xixa
#---------------------------------------------------------
ERR_NARGS=1
ERR_MES=2
# 1) Valida que hi ha un argument
if [ $# -ne 1 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 mes"
  exit $ERR_NARGS
fi
# 2) Validar mes [1-12]
if ! [ $1 -ge 1 -a $1 -le 12 ]
then
  echo "Error: mes $mes no válid."
  echo "Usage: $0 mes"
  exit $ERR_MES
fi
# 3) xixa
case $1 in
	"1"|"3"|"5"|"7"|"8"|"10"|"12")
		echo("$1 té 31 dies");;
	"2")
		echo("$1 té 28 dies");;
	*)
		echo("$1 té 30 dies");;
esac
exit 0

#! /bin/bash
#@edt ASIX-M01 Curs 2023-2024
#Febrer 2024
#Exercici 5 d'scripts bàsics
#Descripció:Mostrar línia a línia l’entrada estàndard, retallant només els primers 50 caràcters.
#---------------------------------------------------------
ERR_NARGS=1
#1) Valida que hi ha 1 o més arguments
if [ $# -eq 0 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args"
  exit $ERR_NARGS
fi
#2) xixa
num=1
while read -r line
do
  echo "$line" | cut -c 1-50
done
exit 0

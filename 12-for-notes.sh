#! /bin/bash
#@edt ASIX-M01 Curs 2023-2024
#Febrer 2024
#for-notes nota...
#	suspès, aprovat, excel·lent
#
#---------------------------------------------------------
ERR_NARGS=1
ERR_NOTA=2
# 1) Valida arguments
if [ $# -eq 0 ]; then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 nota"
  exit $ERR_NARGS
fi
#2) Iterar la llista d'arguments
for nota in $*
do
  if ! [ $nota -ge 0 -a $nota -le 10 ]; then
	echo "Error: nota $nota no vàlida (0-10)" >> /dev/stderr
  else
	if [ $nota -lt 5 ]; then
		echo "$nota suspès"
	elif [ $nota -lt 7 ]; then
		echo "$nota aprovat"
	else
		echo "$nota notable"
	fi
fi
done
exit $ERR_NOTA
#3) xixa


#! /bin/bash
#@edt ASIX-M01 Curs 2023-2024
#Febrer 2024
#Exercici 6 d'scripts bàsics
#Descripció:Fer un programa que rep com a arguments noms de dies de la setmana i mostra
#quants dies eren laborables i quants festius. Si l’argument no és un dia de la
#setmana genera un error per stderr.
#Exemple: $ prog dilluns divendres dilluns dimarts kakota dissabte sunday
#---------------------------------------------------------
ERR_NARGS=1
#1) Valida que hi ha 1 o més arguments
if [ $# -eq 0 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args"
  exit $ERR_NARGS
fi
#2) xixa
dia=$1
laborable=0
festiu=0
for dia in $*
do
 case $dia in
  "dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
	  ((laborable++));;
  "dissabte"|"diumenge")
	  ((festiu++));;
 
  *)
      echo "Error: ($dia) no és un dia de la setmana vàlid" >> /dev/stderr ;;
 
 esac
done
echo "Hi ha $laborable dies laborables."
echo "Hi ha $festiu dies festius. "
exit 0


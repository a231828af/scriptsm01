#! /bin/bash
#@edt ASIX-M01 Curs 2023-2024
#Febrer 2024
#
#llistar el directori rebut
#	a) verificar rep un arg
#	b) verificar que és un directori
#---------------------------------------------------------
ERR_NARGS=1
ERR_DIR=2
# 1) Valida que hi ha un argument
if [ $# -ne 1 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 directori"
  exit $ERR_NARGS
fi
# 2) Valida que és un directori
if [ ! -d $1 ]	
then
echo "Error '$1' no és un directori"
  echo "Usage: $0 dir"
  exit $ERR_DIR
fi
# 3) xixa
ls $1
exit 0

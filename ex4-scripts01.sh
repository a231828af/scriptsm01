#! /bin/bash
#@edt ASIX-M01 Curs 2023-2024
#Febrer 2024
#Exercici 4 d'scripts bàsics
#Descripció:Fer un programa que rep com a arguments números de més (un o més) i indica per
#a cada mes rebut quants dies té el més.
#-----------------------------------------------------------------------------------------
ERR_NARGS=1
#1) Valida que hi ha 1 o més arguments
if [ $# -eq 0 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args"
  exit $ERR_NARGS
fi
#2)xixa
mes=$1
for mes in $*
do
 case $mes in
  "1"|"3"|"5"|"7"|"8"|"10"|"12")
       echo "el mes $mes té 31 dies";;
  "4"|"6"|"9"|"11")
       echo "el mes $mes té 30 dies";;
  "2")
       echo "el mes $mes té 28 dies";;
   *)
	   echo "Error: mes ($mes) inválid" >> /dev/stderr
 esac
done

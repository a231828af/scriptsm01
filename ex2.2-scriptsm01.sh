#! /bin/bash
#@edt ASIX-M01 Curs 2023-2024
#Febrer 2024
#Exercici 2.2 d'scripts bàsics
#Descripció: Processar els arguments i comptar quantes n’hi ha de 3 o més caràcters.
#------------------------------------------------------------------------------------------------------
ERR_NARGS=1
#1) Valida que hi ha 1 o més arguments
if [ $# -eq 0 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args"
  exit $ERR_NARGS
fi
#2) XIXA
comp=0
for arg in $*
do
	 echo $arg | grep -E ".{3,}" 
	  if [ $? -eq 0 ]; then
		((comp++))
	  fi
done
echo $comp
exit 0

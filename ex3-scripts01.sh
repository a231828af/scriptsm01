#! /bin/bash
#@edt ASIX-M01 Curs 2023-2024
#Febrer 2024
#Exercici 3 d'scripts bàsics
#Descripció:Fer un comptador des de zero fins al valor indicat per l’argument rebut.
#---------------------------------------------------------
ERR_NARGS=1
#1) Valida que hi ha 1 ARGUMENT
if [ $# -ne 1 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args"
  exit $ERR_NARGS
fi
#2) xixa
num=0
MAX=$1
while [ $num -le $MAX ]
do
  echo "$num "
  ((num++))
done
exit 0

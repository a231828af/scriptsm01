#! /bin/bash
#@edt ASIX-M01 Curs 2023-2024
#Febrer 2024
#Exercici 2.4 d'scripts bàsics
#Descripció:Processar stdin cmostrant per stdout les línies numerades i en majúscules.
#-----------------------------------------------------------------------------
#xixa
num=0
while read -r line
do
	echo "$num: $line" | tr 'a-z' 'A-Z'
	((num++))	
done


#! /bin/bash
#@edt ASIX M01-ISO
#prog dir
#rep un arg i és un directori i es llista
#--------------------------------------------------
ERR_NARGS=1
ERR_DIR=2
#1) validar que hi ha un arg
if [ $# -ne 1 ]; then
	echo "Error: num args incorrecte"
	echo "Usage: $0 arg"
	exit $ERR_NARGS
fi
#2) validar que és un dir
dir=$1
if  [ ! -d $dir ]; then
	echo "Error: $1 no és un directori"
	echo "Usage: $0 dir"
	exit $ERR_DIR
fi
#3) xixa: lliurar
ls $dir
exit 0

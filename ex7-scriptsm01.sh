#! /bin/bash
#@edt ASIX-M01 Curs 2023-2024
#Febrer 2024
#Exercici 7 d'scripts bàsics
#Descripció:Processar línia a línia l’entrada estàndard, si la línia té més de 60 caràcters la
#mostra, si no no.
#---------------------------------------------------------
#1) Programa
while read -r line
do
  character=$(echo $line | wc -c )
  if [ $character -gt 60 ]; then
  echo "$line"
  fi
done
exit 0

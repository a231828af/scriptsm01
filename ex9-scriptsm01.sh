#! /bin/bash
#@edt ASIX-M01 Curs 2023-2024
#Febrer 2024
#Exercici 9 d'scripts bàsics
#Descripció:Fer un programa que rep per stdin noms d’usuari (un per línia), si existeixen en el
#sistema (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra
#per stderr.
#-----------------------------------------------------------------------------------------
#1) PROGRAMA
while read -r line
do
  grep "^$line:" /etc/passwd &> /dev/stdout
  if [ $? -eq 0 ]; then
	  echo $line
  else
   echo $line >> /dev/stderr
  fi
done

#! /bin/bash
#@edt ASIX-M01 Curs 2023-2024
#Febrer 2024
#
#prog file
#	a) verificar rep un arg
#	b) verificar que és un dir,file,link
#indicar si dir és: regular, dir, link o altra cosa
#---------------------------------------------------------
ERR_NARGS=1
ERR_DIR=2
# 1) Valida que hi ha un argument
if [ $# -ne 1 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 directori"
  exit $ERR_NARGS
fi
# 2) xixa

fit=$1

if [ ! -e $fit ]; then
  echo "'$fit' no existeix"
  exit $ERR_NOEXIST
elif [ -f $fit ]; then
  echo "'$fit' és un regular file"
elif [ -h $fit ]; then
  echo "$fit és un link"
elif [ -d $fit ]; then
  echo "$fit és un directori"
else
  echo "$fit és una altra cosa"
fi
exit 0

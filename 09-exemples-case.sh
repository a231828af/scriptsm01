#! /bin/bash
#@edt ASIX-M01 Curs 2023-2024
#Febrer 2024
#Exemples case
#---------------------------------------------------------
#
#2) dl dt dc dj dv ----> laborables
# ds dm ---> festiu
case $1 in
  "dl"|"dt"|"dc"|"dj"|"dv")
      echo "$1 és un dia laborable";;
"ds"|"dm")
      echo "$1 és un dia festiu";;
 
  *)
      echo "això ($1) no és un dia";;
 
esac
exit 0

#exemple vocals
case $1 in
  [aeiou])
    echo "$1 és una vocal"
    ;;
  [bcdfghjklmnñpqestvwxyz])
    echo "$1 és una consonant"
    ;;
   *)
    echo "$1 és una altra cosa"
    ;;
esac
exit 0

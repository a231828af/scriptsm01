#! /bin/bash
#@edt ASIX-M01 Curs 2023-2024
#Validar que té exactament 2 arguments i mostrar-los
#
#
#04-validar-arguments nom cognom
#--------------------------------------------
#1) valida que hi ha 2 arguments

if [ $# -ne 2 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 nom cognom"
  exit 1

fi

#2) xixa que els mostra

nom=$1
cognom=$2

echo "nom: $nom"
echo "cognom: $cognom"
exit 0

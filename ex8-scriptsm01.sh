#! /bin/bash
#@edt ASIX-M01 Curs 2023-2024
#Febrer 2024
#Exercici 8 d'scripts bàsics
#Descripció:Fer un programa que rep com a argument noms d’usuari, si existeixen en el sistema
#(en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per
#stderr.
#----------------------------------------------------------------------------------------------
ERR_NARGS=1
#1) Valida que hi ha 1 o més arguments
if [ $# -eq 0 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args"
  exit $ERR_NARGS
fi
#2) PROGRAMA
for user in $*
do
  grep "^$user:" /etc/passwd &> /dev/null
  if [ $? -eq 0 ]; then
    echo "$user"
  else
    echo "$user no existeix al sistema" >> /dev/stderr
  fi
done

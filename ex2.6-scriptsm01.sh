#! /bin/bash
#@edt ASIX-M01 Curs 2023-2024
#Febrer 2024
#Exercici 2.6 d'scripts bàsics
#Descripció:Processar per stdin línies d’entrada tipus “Tom Snyder” i mostrar per stdout la línia en format → T. Snyder.
#-------------------------------------------------------------------
#1) XIXA
while read -r line
do
	nom=$(echo $line | cut -c 1)
	cognom=$(echo $line |cut -d ' ' -f2)
	echo $nom.$cognom
done

